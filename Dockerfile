FROM harbor.glythium.io/custom/python

# We'll use root for a bit to set up the container environment
USER root
RUN apk update && apk upgrade --prune --force-refresh
RUN mkdir /pyserver/
RUN chown 1001:1001 /pyserver

# Switch to the non-privileged user
USER 1001

WORKDIR /pyserver/
COPY src/ src/
COPY frontend/ frontend/

EXPOSE $SERVER_PORT

ENTRYPOINT [ "python3" ]
CMD [ "/pyserver/src/server.py" ]