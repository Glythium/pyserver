import unittest

from os import environ
from src.server import is_valid_port, check_env_vars


class TestServer(unittest.TestCase):

    def test_invalid_port(self):
        self.assertFalse(is_valid_port(-1))
        self.assertFalse(is_valid_port(0))
        self.assertFalse(is_valid_port(65536))
        self.assertFalse(is_valid_port(100000))

    def test_valid_port(self):
        self.assertTrue(is_valid_port(1))
        self.assertTrue(is_valid_port(443))
        self.assertTrue(is_valid_port(65535))

    def test_check_env_vars(self):
        with self.assertRaises(Exception):
            check_env_vars()
        
        environ["SERVER_PORT"] = "8080"
        with self.assertRaises(Exception):
            check_env_vars()

        environ["HTML_DIR"] = "./frontend"
        check_env_vars()


if __name__ == '__main__':
    unittest.main()