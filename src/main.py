from server import *


def main():
    try:
        check_env_vars()
    except Exception as e:
        print(e)
        exit(1)

    try:
        port = int(getenv("SERVER_PORT"))
    except ValueError as e:
        print(e)
        exit(1)

    if is_valid_port(port):
        start_server("127.0.0.1", port)


if __name__ == "__main__":        
    main()