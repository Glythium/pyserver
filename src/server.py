from http.server import SimpleHTTPRequestHandler, HTTPServer
from os import getenv


# Override the directory of the basic handler
class Handler(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=getenv("HTML_DIR"), **kwargs)


def is_valid_port(number):
    return number >= 1 and number <= 65535


def check_env_vars():
    for var in ["SERVER_PORT", "HTML_DIR"]:
        if getenv(var) is None:
            raise Exception("'{}' not defined".format(var))


def start_server(hostname, port):
    webServer = HTTPServer((hostname, port), Handler)
    print("Server started http://{}:{}".format(hostname, port))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")


if __name__ == "__main__":        
    print("This is a library, execute main.py to start the server")